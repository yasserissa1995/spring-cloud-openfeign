package com.yasser.microservice2.exceptions.type;

public class FormMetaStructureException extends RuntimeException {

	public FormMetaStructureException(String message) {
		super(message);
	}

}
