package com.yasser.microservice2.exceptions.type;

public class EmailAlreadyUsedException extends RuntimeException {

	public EmailAlreadyUsedException(String message) {
		super(message);
	}

}
