package com.yasser.microservice2.exceptions.type;

public class ResourceAlreadyExists extends RuntimeException {

	public ResourceAlreadyExists(String message) {
		super(message);
	}

}
