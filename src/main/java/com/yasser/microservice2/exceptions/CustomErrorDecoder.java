package com.yasser.microservice2.exceptions;

import com.yasser.microservice2.exceptions.type.BadRequestException;
import com.yasser.microservice2.exceptions.type.ResourceNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String methodKey, Response response) {

		switch (response.status()) {
			case 400:
				return new BadRequestException("bad request ex");
			case 404:
				return new ResourceNotFoundException("not found ex");
			default:
				return new Exception("Generic error");
		}
	}
}