package com.yasser.microservice2.feign;

import com.yasser.microservice2.config.FeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "accountFeignClient", url = "http://localhost:9091"
		, configuration = FeignClientConfiguration.class)
public interface AccountFeignClient {

	@GetMapping(value = "/api/accounts",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	List<String> findAll();

}
