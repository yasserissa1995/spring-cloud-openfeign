package com.yasser.microservice2.controller;

import com.yasser.microservice2.feign.AccountFeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AccountResource {

	private final AccountFeignClient accountFeignClient;

	public AccountResource(AccountFeignClient accountFeignClient) {
		this.accountFeignClient = accountFeignClient;
	}

	@GetMapping("/accounts")
	public List<String> findAll() {

		return accountFeignClient.findAll();
	}
}
