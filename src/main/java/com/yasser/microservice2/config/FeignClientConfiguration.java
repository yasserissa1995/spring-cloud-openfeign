package com.yasser.microservice2.config;

import com.yasser.microservice2.exceptions.CustomErrorDecoder;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;
import feign.form.FormEncoder;
import feign.okhttp.OkHttpClient;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Configuration
public class FeignClientConfiguration {

	@Autowired private ObjectFactory<HttpMessageConverters> messageConverters;

	@Bean @Primary @Scope(SCOPE_PROTOTYPE) Encoder feignFormEncoder() {
		return new FormEncoder(new SpringEncoder(this.messageConverters));
	}

	@Bean public ErrorDecoder errorDecoder() {
		return new CustomErrorDecoder();
	}

	@Bean
	// to use okhttpClient instead of the default one to support HTTP/2. , Feign supports multiple clients for different use cases, including the ApacheHttpClient
	public OkHttpClient client() {
		return new OkHttpClient();
	}


}